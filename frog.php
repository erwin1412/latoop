<?php
require_once "animal.php";

class Frog extends Animal
{
    public $skill = "hop hop";
    public function jump()
    {
        echo "Jump: " . $this->skill . "<br><br>";
    }
}
