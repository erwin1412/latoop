<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas OOP PHP</title>
</head>

<body>
    <?php
    echo "<h2>Release 0</h2>";

    require_once "animal.php";
    require_once "frog.php";
    require_once "ape.php";

    $sheep = new Animal("shaun");
    $sheep->set_legs(2);
    $sheep->set_cold_blooded("false");

    echo "Name: " . $sheep->name . "<br>"; // "shaun"
    echo "Legs: " . $sheep->legs . "<br>"; // 2
    echo "Cold Blooded: " . $sheep->cold_blooded . "<br>"; // false

    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

    echo "<h2>Release 1</h2>";

    $sungokong = new Ape("kera sakti");
    $sungokong->set_legs(2);
    $sungokong->set_cold_blooded("false");

    $kodok = new Frog("buduk");
    $kodok->set_legs(4);
    $kodok->set_cold_blooded("true");

    echo "Name: " . $sungokong->name . "<br>";
    echo "Legs: " . $sungokong->legs . "<br>";
    echo "Cold Blooded: " . $sungokong->cold_blooded . "<br>";
    $sungokong->yell();

    echo "Name: " . $kodok->name . "<br>";
    echo "Legs: " . $kodok->legs . "<br>";
    echo "Cold Blooded: " . $kodok->cold_blooded . "<br>";
    $kodok->jump();


    ?>
</body>

</html>