<?php
require_once "animal.php";

class Ape extends Animal
{
    public $yell = "Auooo";
    public function yell()
    {
        echo "Yell: " . $this->yell . "<br><br>";
    }
}
